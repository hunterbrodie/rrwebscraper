Father coughed. Wetly. Like there was goop sloshing in his throat.

I blinked up from my medicine textbook. Mother had told me to study it, regardless of my choice in profession. It was good to be skilled in all things, she said, although I was sure she was still trying to make me change my mind. To have me awaken to a love for medicine and choose it over music.

Well, it wasn’t like I hated alchemy anyway. It was enjoyable like music in its own way—it had its own rules and intricacies, alongside its share or frustrations. So, I shrugged and continued studying under her. Medicine was useful, after all. Especially for times like now.

“That sounds like a bad cough,” I told father, putting the book down in concern. “Are you feverish, father?”

“Mm, no,” he said, waving off my concern and coughing again. “It’s just Smith’s Lung. Normal for a workman my age. My old master said that all the smoke we breathe turns to tar in the throat—making it sticky. Kind of like your mother’s work with the vapors. It’s harmless.”

I nodded, relieved. If father said it was fine, then it was. He was always right about those things. Still, the cough was bothering him for sure.

“It doesn’t sound like it’s very pleasant,” I said.

He shrugged, then paused. Father smiled wryly, “No, I guess it isn’t.”

“Hold on,” I said, and stood up. I headed for the kitchen, where mother kept her mundane materials. “I’ll make you something to clear your throat with.”

“You really don’t have to.”

“But I will, anyway.”

I heard him chuckle from the couch. He grinned my way, “Are you coddling your old man, Rowan? I may be old, but I’m not weak, you know.”

I paused at that. My hands stopped searching the shelves as I gave him a look.

“You’re old?”

“I’m sixty-three, son.”

“But mother’s ninety-seven.”

He laughed, “Amarids live for longer than trolls. That means you’ll be strong and healthy for far longer than others.” Father stood from the couch and approached, joining me in the kitchen. “You should really thank your mother for that.”

I turned away from him and went back to searching. “Mother doesn’t like me.”

Father scoffed. “You don’t think that. Your mother loves you.”

“She dislikes music,” I said. “It’s the same as disliking me.”

Father didn’t reply to that. He usually didn’t—when he knew I would realize I was wrong eventually. It would be irritating if I didn’t look up to him so much. Instead, it stung. I shook my head, going through the clay jars. This wasn’t the same as smithing. He was the master there; he knew the forge. But I knew mother.

She was far too strict to let me waste my time with music. Not when I was talented in other things like alchemy. And she was always so busy. So grumpy.

Why start a family when you don’t have the time for them? I didn’t get it.

As long as she stayed that way, we would never get along.

As if sensing the sullen train of thoughts going through my brain, father broke the silence. “There are a lot of jars here,” he said, reaching into the cabinet and pulling one out. He opened the lid and sniffed. Made a face. “Are you sure this is medicine? It smells rotten.”

I glanced at the jar he held. Fermented Hogspop.

I waggled my fingers at him. “That’s because it is.”

Father wrinkled his nose, “Eugh,” and put it away. “You better not make me drink that. I’m sick enough as it is.”

“Mother used to put those in your favorite pickles.”

“The ones we’re having with elk for dinner?”

“Yep. And last night, too.”

He gave me a look that told me I’d ruined those pickles for him forever. I smirked. “Don’t worry, father. You’ve got a troll’s body, a genius alchemist for a wife, and a son that takes after you both. You’d be fine even if I got both of us poisoned. Is mother coming home this week? Her letter said she would visit soon.”

“You gained a tongue as soon as you turned fifteen. Where did my bright-eyed son go?” he muttered, sulking, then paused. Father sighed and shook his head. “No, your mother isn’t coming. She’s—”

“Busy,” I cut him off. I didn’t want to hear it. “Like always.”

Father smiled sadly, “Yes.”

I stopped, surprised. I didn’t expect him to agree. He usually defended her—said her work was important or that she was doing it for us. I turned to him and I saw father’s face. Wrinkled. Aged. The crags of stone on his face were weathered and discolored, and his eyes had dark circles sagging underneath.

He was still tall, still strong enough to shatter metal with a hammer, but…

Father was old.

I turned away. Nodded silently.

We didn’t exchange any more words after that. I plucked salt and honey and brineseed extract from the shelves. Mixed them into a drink. Then, I poured carcilla dust into a pot of boiling water and threw several ingredients into the mix. Zamek. Babon leaves. Crushed kiki seeds.

“Your mother was right about your talent,” father said, watching me work. “You work like her. Confident. Sure. I’ve never been much into ‘chemy—it frightens me. One wrong ingredient and you could kill an innocent customer. Rot them out from the inside. I saw it happen, once. Back when I was still a traveling smith.”

Father leaned forward on the counter and stared into the bubbling cauldron over the fire.

“It’s a scary craft, ‘chemy. And it’s full of scarily intelligent people. It’s not for a simple man like me.”

I tapped a measuring spoon against the rim of a glass, letting the powder fall and mix into his drink. “If you dislike alchemy, then why did you marry mother?”

He smiled at that.

“Because she’s the only alchemist I’ve ever learned to trust.”

Huh. I raised an eyebrow at him, “And me?”

“Well she taught you, didn’t she?”

And that was all that needed to be said. Mother was a genius, and that meant I was meant to be one, too. Her student couldn’t possibly be anything less. As much as I disliked admitting it, mother was good at her job. Excellent. Being a master was why she was always so busy.

I remembered the man in silken robes that had come to ask her for help, just over a year ago. He had crimson leaves for hair—speckled with gold. A battalion had accompanied him into the town. I still remembered them, with their livewood armors and artificed gear, strolling into the town as nothing in it could possibly pose a threat. That kind of procession didn’t follow just anyone. Everyone knew what their presence meant.

Royalty. A saer had come to the town to find my mother. A prince of the Summersky house, seeking the acclaimed Elanah Kindlebright’s assistance.

And she hadn’t returned since. I watched the fire crackle in silence.

“…Do you really think she’ll come back, father?” I asked.

Father took the hot drink that I’d prepared and gulped it down. The water didn’t scorch his throat, even when boiling. Father set the glass down with a tak.

“Do you know how your mother and I met, Rowan?”

“Under a rowan tree,” I sighed, exasperated. I’d heard the story a million times already: how they met and how they named me after the memory—after the tree, and my hair that resembled it. But it seemed father wasn’t done.

“True,” he said, “but under that tree was also the last time we saw each other until nine years later.”

Huh?

“I thought you knew each other from childhood?”

“We did, yes,” father nodded. “But when I met her again, I was prenticed to a welder in Jahenna. She was a student, then. At one of the Great Colleges. We recognized each other in a bar, and she invited me for a drink. It had good mead, that bar. It was your mother’s favorite.”

Father seemed to lose himself in the memories for a moment. He smiled wryly to himself. The firelight flickered in the reflection of his golden eyes, looking into the past. Father shook his head.

“We talked for a few hours. A good, pleasant few hours. And when it was time for goodbyes, I jokingly asked her, ‘Will you disappear for nine years again?’ and she laughed. ‘No,’ she said. ‘Not for that long.’ And she disappeared for five years, as if to prove a point. Can you believe that?”

Father laughed at that, as if it were a joke only he and mother could understand. I could only stare in conflicted silence as he continued.

“I met her again as an independent smith, working orders for the Duskwater House’s army. It was during the last Convergence War, and she worked as a personal physician to the House’s head. She walked into my forge, smiling, and the first thing she said—ha!” father barked out a laugh, then coughed, covering his mouth. Father wiped his palm against his smith’s apron and smiled. “She said, ‘I told you it wouldn’t be that long,’ and I couldn’t help but agree. It wasn’t like she lied, after all. We stayed together for longer that time. She visited my workshop daily, and I sent her metal figures at night. Little birds, flowers, and figures, sculpted from what metal I could spare.

“When the war ended, she told me she would set off again. Research, she said. To cure what cannot be cured. So, I changed my question. I asked, ‘When will I see you again?’ and she said, ‘When you’ve decided to make me stay.’ And that’s what I did. I looked for her, and two years later, I found her in the Shadewoods with a party of researchers, hiding from monsters that they only talk about in storybooks. When we escaped with our lives, I proposed. She said yes. We had you. Now—years later, when she decided to leave and help the Summersky House, do you know what I asked her?”

I stared at him. Waiting for him to continue. Father looked out the window and smiled.

“Nothing. I didn’t ask her anything this time,” he said. “I’ve been with her for twenty years, son. And known her for even longer. In that time, I learned that the questions I asked before were pointless. I told myself that I wouldn’t ask them again. Even if I wanted to. No—this time, I just told her to be safe when she came back.”

I frowned, “You didn’t even try to stop her?”

Father put a hand on my shoulder. “Your mother is awkward, terrible with relationships, and has a temper like a summer wildfire. She comes, goes, returns like a storm, and to stop her would be the biggest mistake of my life. She’s a free spirit, Rowan. But she isn’t a cruel one. I trust her.”

I gave my father a look, and he laughed his booming laugh.

“I know, I know. Go ahead and say it.”

Well, if he insisted.

“…That’s kind of sad, father.”

“Is it? I think being in love with the wind is an interesting life choice. Especially for a boring smith like me.”

“You think mother will come back, then?”

“She will. And things will be better when she does. They always are.”

 



 

Mother didn’t come back.

Or if she was, then she was taking her sweet time at it. Two years passed after she left, and father’s condition worsened. He coughed harder, and the sound of it was like his throat was a dry, shriveled leaf to be shattered by a passing breeze. I did my best with what I could, but his malady was no common ailment.

Smith’s Lung, they called it. As if it were a common cough.

But as the symptoms worsened, they began to resemble another condition more and more. The wracking coughs, the shortness of breath, the bloody phlegm—I was certain. It was an illness that no one could be saved from. A killer that took more lives than war and destroyed more families than debt.

Cancer.

Father was dying, and I could do nothing but play songs to help him sleep. He was there now, downstairs, slumbering while a tumor slowly devoured his right lung. The healer said his time was short. That he could leave at any moment.

Seventeen years old, and my fingers could dance against the bansuri.

The sounds came easily to me now—like old friends being beckoned over with each breath and whistle of the wind. I played a sequence of chords, and they spoke in words that I couldn’t say. This tune was the ocean, with its crests and falls, like dropping waves and turning tides. Slow, deep. A song with an energy to its sound that portrayed the vastness of the sea.

I’d never been to an ocean in person. Caereith, my home, had none.

But the books told me where they were. What they were. Big bodies of salty water, a vast blue like the sky, marred by slashes of white foam lapping at the shore. That gave me my image.

And the Singing Tree had shown me the rest.

It let me feel the sensation of riding a boat. Of jumping into the ocean and sinking with your eyes closed, letting the water take you into its depths. It let me feel the vastness of the greater world. The gargantuan beauty of the ocean deep.

I thought of my father as I played, the song taking shape. If I listened closely, the sound of waves accompanied my playing. Faint shoreline ambience, dancing along to the sound of the bansuri’s rasps and chirps. A little bit of magic. A little bit of the world dancing with me as I played. It always helped me put father to sleep.

A neat trick. One I figured out from listening to the Singing Tree.

The tree was quiet now, most days. Like it was dying. Weakening like father. On days where I felt hopeless, I came to it and sat. During those days, it always answered. The Singing Tree was always there for me when I needed it.

But it wouldn’t be forever. I knew that now, now that father was withering away below. People weren’t permanent, and they would eventually go away, no matter how much you wanted to cling on and scream for them to stay. So I tried to learn from the tree while it was still there, just as I learned my father’s techniques. To copy its style of music. To copy the sound of his hammer’s ring.

What I learned from them gave my music life. A will of its own. It turned a song into a dance between me and the personality of the sea, pulling and swaying, creating a chorus, then a verse. A living song with lyrics that couldn’t be sung by man. Words were far too fragile—too simple to represent what I wanted to say. To try would be to betray the song.

To lose the meaning in translation.

Then, as I fingered over the holes, something in the distance answered.

An oud. A set of strings played on a distant rooftop. It was the bass to my lilting tune, the second voice to my song. The player in the distance played their instrument with reservations, as if asking for permission to join me.

I answered with a welcoming slide, hoping that the meaning would reach.

It did.

The second instrument answered again, and it played to my song’s magic, twinkling freely to the tune of the sea. I smiled, opening my eyes to stare out into the moonlit street outside.

Whoever it was playing, they understood.

They knew I was sad, and that I was trying to be happy. They knew my struggle. My love for my father. My love for music. They heard it in my instrument—in the beat of waves and the rushing of water that accompanied the sound of it.

I stood up from my bed, playing my bansuri, nudging the window open with my foot. I stepped outside, into the balcony. The sound of my instrument carried over the rooftops, accompanied by the singing of waves. I raised the tempo. Tested the waters.

The waves roared back.

The oud—the partner to my song—answered my challenge. It matched my pace easily. It danced around the tune I composed, playfully pulling in and away as if to taunt me back.

Is that all you can do?

I found myself standing up to play better. Far from it.

I played up to the challenge carefully, as to not wake my father downstairs. But that wasn’t right. I pursed my lips.

This wasn’t enough. I needed to play better. Louder. I needed to go outside.

But to leave father when he was so weak? So fragile? I didn’t know what to do. Between the song and the fading life below me, the choice was painful.

But it was easy to make.

I couldn’t stay here. I couldn’t watch father draw his last breath. Perhaps, if I left, I could delay the inevitable. Wise men said things only truly happened when perceived.

If that was true, then I would run.

I had to. More for my sake than father’s.

I didn’t know when, but I’d stepped off the balcony. I played my bansuri as I balanced my weight on the ledge, then hopped down, landing on the roof of my father’s workshop. I brought myself down to the wall and jumped again. I landed on the street; my eyes focused on the direction of my partner.

Whoever was playing was out in the forest. Deep into the trees, hidden behind the towering trunks and crawling fauna. I walked to it, playing through the streets, the dark houses, and the towering, lightless lamps. Out until the ground beneath me turned from cobble to stone to dirt.

As I approached, the sound of the oud increased. Became closer. But whenever I passed a brush, certain that I would find whoever was playing, the sound was suddenly farther away, in a different direction. It twinkled and flickered like a wisp in the night, beckoning me to follow. I did.

“Who are you?” I asked, pausing for a moment to speak into the trees, stepping over gnarled roots and moss-laden stones. “What’s your name?”

The oud turned playful, the source just out of sight.

If you want an answer, you’ll have to find me first, it seemed to say. Instead of words, I answered with an insistent set of notes, hissing my curiosity into the trees.

Stop running, then! Let me catch you!

A reply in strings. A laugh, a titter in the music. Another challenge.

Chase faster.

I did. I found myself moving as fast as I could while playing. I hopped over the roots and ducked under branches, slipping into the shadows and out into the streams of moonlight breaking the canopy. The tempo of the song was increasing. Without my realizing, my partner had changed it from ‘ocean’ to ‘storm.’

My hands began to hurt, but I didn’t stop. I knew it in me that if I did, the oud would disappear. It said it without words. It spoke it in the hum of its strings and the reverberations in its body.

I knew, deep inside me, that to lose here would be my biggest regret.

So I chased. Played harder. Stronger. I channeled everything into it, building myself into the breaths I took, then exhaling it out into the mouthpiece between my lips. I pushed myself to the limit of what I could do, and something in me changed. The song I played solidified. It was suddenly deeper, more complex, as if a new layer had been added beneath the first.

And out from my bansuri emerged a sound I never thought I would hear from my own playing.

 

.̵̯͓̀.̴̩̓̆͝,̶̥̭͇̽̄̈́;̵̯̂̒̃.̵͚̞̾͊;̶̡̙̓̔͝ͅ'̷̹̂,̷̜͒;̵̨͛'̸̡̩̰͛̈͝.̵̩̅̄̆;̵̣͑̂'̶̧́̀ͅ.̶͙̳̙̋,̷̜͒;̵̨͛'̸̡̩̰͛̈͝.̷͙̈̃̆,̵͓̀̑;̷͕̭̤̈́̂̅'̶̹̩̆̀̅.̵͉̝̮̒͋͋.̶͉̫̊̃͌ͅ,̸͓͆̾̆,̶̹̲̓.̸̛̝͔͉͐̐—

 

A language. A concept. A tongue. An idea. A shape. A form.

So many things, condensed into a single line. A language eternal, unspoken, unheard. As it rang into the trees, the winds changed direction.

The gusts flew towards me, wrapping my limbs in wind. Lightening me.

My speed redoubled. I shot through the forest, as fast and as nimble as galewind song. The bushes and vines that slowed me before gave way, as if giving me permission to pass. The roots below my feet were suddenly not as tall, and the obstructions in my path were suddenly elsewhere, unable to bar my advance.

I played, and the world acquiesced.

Strengthened by the song, I broke through the brush, into a clearing, into the presence of a pond of light. And there, in the middle of it stood a bird. Standing on the water.

I knew who it was the moment I saw it. A little blue bird, now smaller than my fist. A bird of light, listening to my song.

It was the first to listen to my music. The one to give me the push I needed, years ago.

My bluebird. My friend.

It lifted off from the pond and flew up, up to the branches of a tree far taller than the rest. It flew towards a man in brilliant clothing, smooth as silk and soft as cloud, the color of blue and white and gold and green. He had glowing, leaves of solid starlight for hair, flowing to his shoulders, and nebulous eyes that were of a thousand, whirling colors. Rainbows made solid, watching me from above.

“I found you,” was all I could think to say.

“And so you have,” the man smiled, lowering the oud in his hands, the bluebird perched on his shoulder. “How does it feel, child, to be the one who finds the Fae?”