Aami stared at the puddle of melted soil and blinked. Frowning, she turned her gaze to Priscia, “I’m still new to this friend thing, but I’m pretty sure poisoning each other is a mother and son kind of activity.”

Priscia looked up and furrowed her brows, “What kind of mother did you have?”

“None! That’s my other friend’s mother. I don’t have one. I kind of just started existing one day, you know?”

“That’s—wait, are you not mad at me?”

“Why would I be?”

“I literally just tried to kill you.”

If that was the case, she was kind of doing a terrible job at it. Aami gave the puddle of poison soup a glance, then turned her confused gaze to the chef. “You tried to kill me by throwing poison at the ground?”

“No! I was trying to kill you before, because you were tall and pretty and you looked like you’d eat me. But now, you’re only two of three things that terrify me, so…”

“Thanks!” Aami said, breaking out into a wide grin. “So far, all my friendships have started with the other person trying to kill me, so we’re fine. I’ve already been maimed, shredded, and pulverized a couple times. Poison’s nothing.”

“Wow. I’m really sorry to hear that.”

Priscia gave her a pitying look, but Aami only waved it off with a laugh. “I came into my agreement with Beardy knowing that getting to know people would be hard. I wouldn’t even be surprised if you shot me a few times for good measure. I’d be a little discouraged, but…” she shook her head, and her look turned wry. “I’ve had plenty of that already. But we’re cool now, aren’t we? You threw the poison away.”

The amarid blinked, “I guess? This is really weird. I’ve never tried to poison someone before. I didn’t expect you to be this nice about it.”

“It was never really dangerous to begin with. I just made organs in this body for fun, so it doesn’t matter if I lose them. I can grow them back; no biggie.”

“I still feel a little guilty, though.”

Aami smiled and took the girl’s hands into her own, “In that case, I know exactly how you can make it up to me.”

Priscia blinked up at her, “How?”

“How about helping me make friends?"

 



 

Dinner at the camp was a restless thing. The lack of attacks from the Crimson Tide wreathed the outpost in an unsettling silence—peace, in a place that should have none. The sounds of cannon fire that were usually ever-present gave way to a hollow quiet that made the soldiers shift atop the walls in discomfort. Aami watched them from the camp’s outdoor kitchen, nervously pacing about as Priscia dumped ingredients into a stew. She turned to the chef with a troubled look, “When I said introduce me to people, I meant maybe one at a time! Not the whole camp!”

Priscia sipped at a ladle and shrugged, “I’m not close enough with anyone here to introduce them to you. So we’re using cook privilege. I get to make the rounds handing out food and you get to come with me.”

“And then what? We just talk?”

“Of course not. The soldiers are terrified of you. They’ll just freeze up if you talk to them.”

Aami’s shoulders sunk, “So what’s the point? I’ll just be walking around with you and scaring people. No one wants to be scared while eating, Priscia! Food is an enjoyment thing!”

The chef smiled wryly, “No one enjoys food here. We’re in the middle of nowhere, expecting an attack at any moment. My elk stew only makes people nauseous. It looks like the blighted, you know? Being red and goopy and all.”

“I think it’s just the beans. No one likes beans.”

“Well, it’s either beans or nothing. Point is, we aren’t making the rounds to make you friends yet. It’s not that easy. This is just the first step.”

“The first step is scaring people?”

Priscia laughed. With the way Aami’s personality was, getting over the scary eldritch alien impression was surprisingly easy. Priscia couldn’t really bring herself to be afraid anymore, now that she saw what a the girl was like. Aami’s humanoid appearance certainly helped, too, which was the cornerstone of her plan. Priscia started pouring stew into bowls for serving, “No, dummy. They’re already scared. The first step is getting you around the camp so people get used to seeing you here. I was a waitress before I was a cook, and trust me—food, a smile, and a pretty face go a long way.”

She set down a set of steaming bowls atop the push cart and offered Aami a second ladle.

“Here,” she said. “Help me serve these.”

Aami accepted the ladle with a sour look, moving to the massive pot and despondently pouring huge portions into the bowls. “I know I’m not going to have an easy time, but it’s not like I enjoy being stared at like I’m about to eat someone.”

“This’ll put a stop to that after a couple days, trust me.”

“And if it doesn’t?”

“You have full permission to eat me.”

Aami looked up from her bowl and pouted. Priscia laughed. Together, they filled enough bowls for a hundred people and set off, pushing carts full of steaming stew towards the walls. The elevators took them up—up to where the brightblooms grew like white-light torches over the battlements. Urging Aami to follow, Priscia pushed her cart towards a pair of Shissavi looking over the darkness below.

At the sound of their approach, one of the armored soldiers turned to face her. The woman took her helmet off and grinned at Priscia, “You don’t usually deliver our food personally like this. What happened, chef? Did you get a raise?”

“Nothing like that. I’ve just got an assistant with me, so I’m making the rounds to show her around the camp. Go on and give them a serving, Aami.”

Priscia turned her body to show the soldiers Aami, who was behind her. The shoggoth gave the soldiers a strained smile. She reached into the cart and offered a bowl to the woman, pushing it forward with both hands.

“Hello,” she said, bowing her head. “I hope you um, enjoy the meal. Even with the beans in it.”

The woman laughed.

“I don’t like beans, but I’ll make sure to enjoy it since you asked so nicely,” the soldier replied, reaching out to accept the bowl. She turned her smile to Priscia. “Who’s your new partner? An apprentice from Felzan? I haven’t seen her around camp.”

Priscia waved a hand to the side, “She’s the black thing that destroyed the blight this morning. The Black Death’s a shapeshifter, remember?”

The soldier’s hand froze under the bowl as realization dawned. She eyed Aami and nodded, slowly, as the guard behind her paled. She pulled the bowl back towards herself. Aami felt herself fidget under the gaze of both Shissavi, but Priscia was kind enough to step forward again, blocking her from view.

The chef handed the second soldier his own serving. One smaller than Aami’s. He stared at it.

“What?” she asked, raising an eyebrow. “Did a stinkbug slip into the stew or something?”

“N-No.”

He took the bowl with a wary look, and Priscia nodded. She pushed her cart forward and motioned for Aami to follow again. The girl in question scurried after her, keeping her head lowered as she wheeled her cart past the two soldiers.

She felt their stares on her back the entire time.

The two of them traveled down the battlements, towards the next pair of watchers. But with an eye open at the back of her head and tucked under her hair, Aami watched the two as she left. She saw the man glance at his partner’s bowl, then stare down at his. He frowned at the stingy portions.

“…How come the Black Death gives bigger servings than the chef?”

 



 

The rest of the night passed in much the same way. Over the next few days, Aami made herself known to practically everyone in the camp. They all stared at her wherever she passed, their eyes all ranging from wary to afraid. Whenever she wasn’t the Black Death, sweeping the blighted land free of threats, she was Priscia’s assistant. Her apprentice, helping the chef cook. Serving food around the camp to people who were clearly reluctant to accept meals from her.

It was depressing. Even downright humiliating at times. Every time she remembered the constipated smiles that she gave the soldiers, Aami felt herself cringe.

Bad, bad, bad! Terrible! Horrible!

She knew how to smile. She saw how people did it, and she’d copied it perfectly. Ancestors, she practiced in front of a mirror daily! It was perfect! But whenever she found herself facing another person, her stupid face always went haywire. It creased in all the wrong places and heated up in shame with every stare she received. It made her want to crawl into a hole and… and…

Aami groaned and buried her face in her hands. She was alone in her room, now. In the nice little tree house that the prince had erected for her to stay in. Sighing, she laid down. Face-first into the marshmallow firmness of her pillow.

“I don’t want to go out anymore,” she muttered, feeling the exhaustion sink deep into the bones she created for her body.

It wasn’t even a physical thing anymore. Her brain felt tired. Her everything felt tired. And she couldn’t even go to Rowan for comfort or advice. No—he was busy. Aami and Priscia had dropped by the lab to deliver meals to him and his mother, but they were always asked to leave it outside the doors. He hadn’t left that room in days! Aami was half wondering if he’d somehow turned himself to stone with some potion. That would explain why he was stuck in there.

But no, this was just like him. Whenever Rowan got preoccupied with something, he always focused on it way too much. Whether it was training, studying, or practicing, he was always so busy.

And now he was too busy for her to come over, hug him, and complain.

Aami sighed. She wanted a hug. Hugs were nice. They were like bundles of warmth and energy—perfect for a recharge on a rainy day. Priscia was getting along with her, but Aami was still scared that she might push the girl away somehow. If she got too touchy, would she hate it? If she started joking with her more, would Priscia dislike her? Was she even funny to begin with?

Agh.

Being a person was so hard. Being hungry all the time was so much easier.

And also boring. So very boring.

She turned over and kicked her legs around the bed, thumping her feet into the soft mattress. Aami hugged the pillow to her face, sighing again. Despite the constant rejections and wary stares, being a person was still so much more fun than her life before. It made her feel alive. Like she was something other than just a patch of emptiness, waiting to eat whatever came near.

Now, she was more than that. Better. Happier. And even if the rest of the soldiers were still afraid, at least she had Priscia now. Aami grinned into the pillow and kicked her legs around again, humming at the thought of her new friend.

Priscia was nice!

She was quiet and awkward at first, but now the girl was a bundle of conversation whenever Aami was around. The two of them could spend all day talking, and Aami wouldn’t mind at all. And as if she weren’t amazing enough already, Priscia was even teaching her how to cook! Aami still had trouble handling a knife as skillfully as her, but being an assistant was tons of fun.

Chop, chop. Mince, dice, season, slice, and fry. Aami laughed into the pillow and rolled around. Salt, sugar, spice. Thyme, potatoes, and rice. So many ingredients, so many ways to cook. And every single one was delicious! Or at least, they were whenever Priscia was the one to prepare them. Aami still had a ways to go before she got to her level. Maybe never.

But that was okay. Because that just meant that Aami could be her student for longer.

Breathing out a long sigh, Aami released a week’s worth of frustration into her pillow. Her breath warmed her face and she closed her eyes. She could understand why people slept, now. Being tired was terrible, and the ability to close her eyes and simply wait the ache away with a good nap was the greatest thing ever.

It seemed she wasn’t going to get a nap quite yet, however.

A knock rapped against the other side of her door.

Blinking, Aami stood up from bed and approached. What was Priscia knocking on her room for? As far as she knew, the girl was usually asleep at this hour, being the early sleeper that she was. Aami had woken her up, once. That was a mistake; she’d gotten scolded for it.

Interrupting sleep was a crime and it made people grumpy. That was clear to her.

So now, in the middle of the night, why was Priscia here to interrupt hers? Aami turned the knob and swung the door open. She poked her head outside.

“What’s wrong, Priscia? Is there… oh.”

Aami paused. It wasn’t Priscia on the other side of the door.

Instead of the chef visiting her little home, it was another familiar face—the female Shissavi from before, days ago on the wall. The first person Aami had ever served food to. The woman had her helmet off, revealing her braided lead hairs falling back over, curved, wooden horns. Droopy, green eyes stared into hers.

“Gud evening, mish Black Death,” she said, her lazy expression lighting up with a nervous smile. Her face was flushed—drunk. Nervous. She scratched the back of her head, “Anceshtors, I didn’t wake you up, did’s I? Shorry if I did. M’really shorry.”

Aami shook her head, confused. Why was she here?

“You didn’t wake me," she said. "You’re Caissa, right? What’s wrong? Are the red goop monsters outside attacking again?”

Caissa shook her head. She glanced back at Aami’s house, then back to her. She gave the shoggoth a sheepish nod. Aami watched the woman bring a bottle out of her satchel, glinting faintly under the blue moonlight. Warm liquid sloshed about inside.

Ale. Caissa held it out to her, and Aami blinked. She pointed a finger to herself?

“For me?”

“Yesh, mish,” the intoxicated Shissavi nodded. “I wus too scared to give it to you at first, but thish ish—” she staggered, blinking blearily, and Aami caught her. The woman gave Aami a drunkard's smile. “—ish thanks. For showing the Crimson Tide who’sh bosh. And also that tasty shtew.”

Aami helped the woman stand back on her feet, before tilting her head. “I thought the Shissavi hated me?”

Caissa shook her head, “I’m a fan of yoursh, mish.”

“Oh,” Aami said. She took the bottle and stared at it, looking at the ale inside. “Thank you.”

“Ish no problem. You saves ush, mish. Thanksh to you, there’ve been no casualties in daysh. The whole camp’sh happy about it, even if most of ush are scared of angering ya. Shrory again.”

Aami didn’t really hear her. She was just staring at her first present. The bottle was shiny. The ale was sweet. It was polished to a sheen, and a ribbon had been tied messily over the cap by what were clearly drunken hands. A warm, bubbly sensation blossomed in her chest and took all the weight away. Aami felt her eyes heat up.

She sniffled. Caissa paled.

“Oh, Ancestorsh. M’so sorry, mish. I knew I wus a damn bother. I didn’t mean to make you cry. M’so stupid. Wait, pleash, let me get you a napkin and—”

The woman fumbled for her pack, reaching inside, but Aami stopped her. She reached out and touched Caissa’s arm, and the Shissavi flinched. She looked up at Aami in apology. But Aami wasn’t angry. She was warm. It was deep in her chest, in her beating heart, warm like a fireside seat. Aami wiped the moisture on her cheeks and sniffled again.

“Sorry,” she said, hugging the bottle to her chest. “I’m just happy. Thank you.”

“Ohh. Erm, do you shtill want a nappy?”

“Sure. Please.”

Caissa handed it to her. Aami blew into it, then turned slightly, opening the door wider. She motioned inside her temporary home. Aami gripped the bottle tight, “Do you want to come inside and drink some of this with me?”

The woman blinked. Grinned, “Can I bring shome of the othersh with me?”

Aami nodded, laughed, and then everything was okay.