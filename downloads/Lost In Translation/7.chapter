I eventually chased the caravan down.

It wasn’t too difficult. They traveled slowly in the afternoon. It seemed that despite their size and agility, the lizards could only travel at a run for a few hours every day. I sat atop one of them, staring at their swaying tails as we tread over the lake.

It was lonely, traveling with a group of people I couldn’t talk to. But at least I could hear their voices. Ancestors, even a musician would get tired of their own voice eventually. And I wasn’t even that good of a singer.

So I guess this would have to do for now. I idly kicked my feet and listened to the conversations around me to pass the time.

“Ashes, this lake is massive,” one of the stocky, bald humans said. “It has to be the size of Southern Astalon, at least.”

The novi at the head of the caravan turned his head, an eyebrow raised.

“You’ve traveled the Grey Wastes, Teban?”

“Aye. You know how my job is—travel, travel, travel. Even danger zones like the Grey aren’t off-limits.”

“Heh. Being a field scholar for the Coalition sounds like more fun than I guessed.”

“I wouldn’t call it fun,” the human scoffed, tugging on a leather belt around his shoulder. He pulled it forward until a book as broad as his chest slid out from underneath his coat. He gave it a proud pat. “But it’s damn interesting. You won’t believe the kind of things I see out there, Xaan. And it’s all recorded in this journal. Years of my genius research. Wanna take a peek?”

The novi glanced at the massive tome and made a face.

“Eh, no thanks. Reading ain’t my thing.”

“Don’t worry, friend. I made sure to put pictures in for people like you." He waggled his fingers, "Colorful ones."

The people in the caravan laughed at that, much to the novi’s chagrin. There wasn’t even a beat of pause as another round of banter began. My eyes weren’t on them, though. I leaned forward on the salamander, eyeing the book from above. A travel journal, was it?

Interesting.

I hopped down from the lizard and approached. Like always, none of them turned to look at me even as I strode into the middle of their conversation. I looked at the human holding the journal.

Teban, his name was. A large man with a balding head. He carried a sack of belongings on him, but the massive journal took up most of his luggage.

Would he notice if I borrowed it? Stupid question.

Why ask when you can just do it?

So I reached out, pulled on the leather strap, and unbuckled it from his shoulder. The journal slid free; right into my waiting hands. I stood there for a moment in silence. Waiting, my breath held to see if he would notice.

I watched, tense, and Teban walked right past me as if his prized journal hadn’t just been nabbed.

I sighed.

...Really, I’m not sure whether I should’ve been relieved or not. I just stole his life’s work, and he didn’t even spare me a glance. Ancestors. This Name thing wasn’t something to scoff at. Whatever the full extent of the effect was, it rendered my existence completely invisible. Even my actions. Rot, I could be the ultimate thief and no one would even know that I stole from them.

I paused at that, nodding. Hm.

That actually didn’t sound so bad.

With a grin, I slung the journal strap over my shoulder and strode back to the lizards. I grabbed onto the reins and pulled myself up. It was comfy up here. Nice and windy. None of the merchants sat here to lessen the load on the lizards, and while I did feel sorry for the poor things, I still refused to walk.

After all, sitting just felt so good.

No way I was passing that up. I planned to sit for as long as I wanted, damn it. Now—to the important things. The journal. Humming a tune under my breath, I reached for the strap on my shoulder, and… nothing. My hand closed around empty air.

Hm?

I furrowed my brows and looked around, but the book wasn’t on me. I didn’t even have the strap over my shoulders. I worried that maybe I’d dropped it going up the lizard, but—I stopped as my eyes reached the rest of the caravan. Teban, in particular. He walked, talking, laughing with a hand hooked against the strap and the massive journal it was attached to.

The journal I was supposed to have.

I stared at it in confusion for a moment before a thought snuck its way into my head. A suspicion. Looking to confirm it, I hopped down from the lizard and approached another human. A guard. I snatched the magitech rifle from his hands and walked off.

Three seconds passed. Five.

I felt the weight in my hands disappear, and the gun was gone. Just like that. I didn’t even have to blink. One second it was there, and the next it wasn’t. I stared at my empty palms in disbelief.

No way.

Turning, I walked to the lizards and grabbed the canvas cloth covering one of the crates. I tore it away. Kicked off the box’s lid. I ruffled around inside, pushing aside bolts of cloth and rolls of silk, before grabbing a little hat from inside. Nice and broad and white, perfect for keeping the sun away. The kind a lady might wear.

I walked away from the lizard, staring at it. Careful to watch its every movement.

It was there for five seconds.

…And then it wasn’t.

It just vanished. Without warning, without so much as a sound. I held it for five seconds, and then it was gone.

“You’ve got to be joking,” I said, turning my head up to the sky. “You’ve got to be fucking joking with me, right?”

Being invisible and unheard was one thing, but taking my ability to interact with things?

That was cruel.

What the hell was I supposed to even do, then? Just watch things happen until I killed myself from boredom? And if it was true that the things I screwed with disappeared, then—

My hand went to my chest, where the little necklace of glass was. The orb full of plants and tiny crawlers, sealed inside in its own self-sustaining ecosystem. My hand closed around it—solid. Reassuring cold glass. Proof of my ability to affect the world.

I forced back the frustration flooding my chest and relaxed.

The orb was there, and the string around it that I tied was present. I released a breath and walked after the caravan, my chin cupped in thought. Once I caught up, I climbed up a lizard’s back once more. I sat there, surrounded by boxes, my lips pursed. 

Out of all things that happened so far, I couldn’t believe that a simple thing like being unable to steal was what made me lose my cool. I was just like a brat who’d bought a toy that didn’t meet his expectations.

And I almost threw a tantrum over it. Like some idiot screaming at the sky.

Seventeen years old and I was still a stupid kid. I sighed.

Venti, who had been perched on my shoulder for a while in silence, pecked at me.

I glared at her. “You’re not the one in the middle of an existential crisis here, Venti. Let me have my moment.”

She flapped her wings and sat back with an exasperated look.

I snorted. Hmph. What did she know?

Arguing with a bird would have to wait until later, though. For now, I needed to focus on the issue at hand, which was my ability to interact with the world.

As far as I knew, I couldn’t take things from people. Stealing wasn’t allowed. If I walked away with something that belonged to another person, it would disappear as if I’d never nabbed it at all. So far, the only other object I’d touched that didn’t disappear was my little trinket. Maybe because it wasn’t in anyone possession?

Or was it because there was no one around when I took it? And what was a Name, anyway? What did it mean to lose it? How did that affect the way I interacted with the things around me? With people?

I frowned.

Too many questions.

And there was only one way to answer them.

Pinching the bridge of my nose, I stood up. I looked around at the caravan. Twenty-three people, seven lizards. All with things I could experiment on.

“Alright, then,” I muttered, holding my bansuri over my shoulder like a stick. “Let’s go see what I can’t do.”

 



 

The caravan stopped at sunset.

With the efficiency of an experienced hand, I watched the travelers set up camp. They set down a plate of metal in the water, and with a twist and a charge of magic, it shoved the water away. It created a perimeter fifty feet across where the lake couldn’t flow, where rest of the water was held at bay by an invisible wall of force. I watched them set up the tents next. Unenchanted ones, by the looks of them. The bare minimum needed to sleep in.

I walked around the camp as they prepared, watching in interest. It was interesting, how quickly these camps could be pieced together and taken apart. Almost optimized.

It still took time regardless of that, however. And it meant dinner came late.

An hour after the sun fell, the caravan was gathered around three smoke-belching fires, tearing into rationed meals and fish caught from the lake. Strangely, I watched one of the humans cook his catch until the meat was white. The poor thing. I shook my head. Some rumors were true, after all.

Fresh meat really was poisonous to the human stomach.

I pitied the guy who had to cook his meals first. Fish was much better when salted and eaten raw. Especially when dipped into sauce.

The human way was a waste. A tragedy in taste.

I sighed and looked around.

Now that I was tired of experimenting, I ambled about in the middle of a group of eating people, pacing around the fire. I tried to ignore how delicious the food around me looked. Or how juicy the sounds coming from each bite was. Thankfully, my sense of smell was gone, and it spared me from further agony.

There were other kinds of pain to keep me occupied, however.

“I can’t believe this girl’s really peeling the skin off an apple,” I said, disgusted as one of the humans sliced up the fruit. She even knifed out the seeds! The best part!

Even Venti recoiled in horror at the sight. I had to cover her eyes—to save her innocence from witnessing any further sin.

Ancestors, these people were wasteful savages.

Did they throw away the heads when they ate insects, too? Or Ancestors forbid—did they roast crickets and larvae over a fire? What about snails? Did they not crunch on the shells either?

Ugh. I didn’t want to find out. It would only lower my opinion of them even more.

Humans were weird. So picky.

Distracting myself from such thoughts, I recalled my findings so far.

Stealing was a no, first and foremost. Taking an item that belonged to someone or a group of people wasn’t allowed, but I was free to pick at things otherwise. Even when I was in plain sight. Grabbing rocks and juggling them in front of the caravan-goers didn’t catch their attention, although throwing it at them was another thing entirely.

A few hours ago, I’d lost my temper again. When one of the amarids refused to look at me—even after I started shouting the lyrics to ‘Randy Riftwalker’ into his ear—it pissed me off.

So I threw a pebble at him.

Immature, yes, but it did contribute to my findings.

I remembered watching the rock fly, then disappear. Right before it hit the back of his head, it teleported back into my hand as if I’d never thrown it in the first place. I frowned at the memory.

It was like some force was preventing me from affecting any of them, even though I could without its interference. It masked my presence and reverted any noticeable changes I made to the world. Was a Name truly so important that I couldn’t do anything without it?

I bit my lower lip. Questions, questions. Ones a stupid kid like me had no answers to.

Tch.

I kicked some dust into the fire, but it returned to the earth before it could smother the flames. For a moment, I considered just walking ahead of the caravan and screwing the path to see if the changes would stick once they reached it, but—

Hm?

My pointed ears perked up.

There was a sound. There, far in the distance. Faint. Persistent. One I’d gotten to know well over my weeks of travel over the lake.

It was the sound of a group wading through the water.

I turned my head north. Into the darkness beyond the fires. My eyes saw only gray, but they pierced through the darkness, showing me the colorless outlines of things in the distance.

They were small. Hunched. They hobbled forward, thirty—no, around fifty of them. I couldn’t make out their faces from this distance, but I saw what they carried. Spears and nets. Some, shields made of crustacean shells as broad as my chest. They moved quietly—almost invisibly to me, even with my dark vision and enhanced hearing.

Now, I was new to the whole traveling deal, but one thing was obvious even to me.

Those things were not here for friendly reasons.

“Hey,” I said, looking around at the people around me. “You idiots are about to get attacked. Look up! North!”

They continued eating. Laughing. Talking.

“You’re outnumbered, morons. Get up and grab your weapons.”

Nothing.

I clicked my tongue. Ancestors damn me, these people were going to get killed at this rate. And as much as their inability to notice my existence frustrated me, I wasn’t going to go and watch them get caught by surprise. Even if they could probably win against those primitive weapons with minimal casualties.

No risking my new travel group. Especially when they knew the area so well.

So fine, then. I’d help. Or try to, at the very least.

I trudged out of the camp, heading towards the figures in the distance. I wasn’t sure what exactly I was going to do, but I’d be heartless if I didn’t at least try. Even if I didn’t find a solution, I would at least be able to see what these guys in the distance were. Curiosity sated and all. Plus, I felt no danger approaching them like this.

It wasn’t like they’d be able to see me, anyway.

I drew closer.

My feet splashed beneath me as I stepped into the water, out of the camp’s perimeter. The ambushers were far, and walking normally would take too long. Even at a jog. So, I brought the bansuri to my lips and played a familiar tune. Galesong. The wind picked up and carried me forward. I bounded over the water and narrowing my eyes, I began to make out the group in the distance.

Glekks. Bipedal fish-men, with yellow eyes and webbed fins. They were apparently the Caereith equivalent of water goblins from Astalon if I recalled correctly. Mother’s books contained a thesis about how their eyes could be used for alchemy. A balancing agent of some sort.

I disagreed. Non-biological materials were always better as balancing agents. They were simpler in composition; less risky when involved with volatile materials. Adding biological mass to highly reactive elements was a recipe for disaster. Risking an arm or two wasn't worth the potential result of experimentation.

I continued my approach with a shake of the head. Alchemy stuff later. Murderous fish-men now.

Getting closer, I noticed that they were only around half my height. That didn’t mean they were any worse off in the water, though. I could see how easily they maneuvered through the shallow, moving at a half-swim, half-crawl. I winced at the sight of them.

Ugly little things. Like someone decapitated a dead fish and glued its head to an infant’s neck. And the smell—ugh. They smelled terrible. Like sun-rotted guts and sour milk.

Venti flew beside me, chirping urgently.

Yes, yes. I know, Venti. These things weren’t pretty. But still, I had to figure out what the hell to do to stop them. Or maybe warn the people in camp somehow. As I was thinking, however, the glekk raiding party ahead of me started rising from the water. They raised they spears. Braced for a throw.

I frowned. Why? They were still too far from camp to atta—

Venti pecked at my face with another chirp.

“Ouch! What—”

Pain.

My eyes widened. I could feel. I could smell. I could taste the rancid fish in the air. My senses were—

The glekk at the front flicked their arms. Claw-point spears blurred towards me. I yelped, flinching, and the music coming from my mouth stopped. The Galesong stopped short. My weight returned. My momentum threw me forward. I tripped. Rolled. Sputtering, spitting, blearily standing from the lake. I heard plops around me. Spears, narrowly missing, then a flash of heat on my side. Pain. A shallow cut.

I sputtered out of the water, coughing and stumbling. Bleeding.

What the hell was happening!? Why now, of all times, could something like these stupid fucking things see—

I opened my eyes to another volley of spears, flying towards me.

I flinched on reflex. Unable to react.

And then a flash of blue appeared in front of me.

Venti.

She chirped, and Galesong emerged from her beak. A gust like a physical blow slammed down from the sky and struck the spears. I saw them crash into the water as if slapped by an invisible giant’s hand. Up ahead, the raiding party of glekks began rising from the water. They gurgled out these disgusting wet croaks—like children drowning in their own vomit. Many of the ugly little bastards leveled another set of spears and charged.

The monsters rushed me, screaming out their choking battle cries. Venti turned to me and chirped. She dove. Pecked at my forehead. The pain registered in a split second, and the jumbled thoughts in my brain halted. My body regained control. My head cleared—

And it told me to run the hell away.

I wasn’t going to fight against an argument like that.

I stumbled up. Turned. I blew hard into my bansuri, and the shrill scream of a panicked song emerged from my instrument. Galesong flooded forward and took effect. But it wasn’t the calm, controlled song that it normally was. Now it was panicked. Clumsy. It rushed to me and I felt weight disappear. A force slammed into me from below. The wind, driving me up, up, up—

—Then I was flying. Falling. I arced in the air, towards the camp—towards the ground. An army of angry fish-men roared behind me. At the sound, the people in the camp stirred and rushed to their weapons, wide-eyed with that stupid, we-just-spotted-the-obvious-threat look.

If they thought the Glekks were surprising, then they would be in for a real sight once I crashed and turned myself into meat paste. Right in the middle of their damn camp. I panicked in the air as the ground blurred towards me, flailing my limbs, Venti too far away to help.

The Galesong got me going, but how was I supposed to stop?

I closed my eyes and did the only thing I was even remotely confident in doing. I played a second song.

My bansuri screeched out another note; sharp and screeching, like jagged metal against stone. The sound of it reverberated into the water and—

I watched the lake below me churn.