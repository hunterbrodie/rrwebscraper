Riftwalker Randy driftin’ drowsy down the dock

Rifle beaten n’ battered from tip down to stock

Raft sails a-tattered, a cracked compass-clock

Rav’nous sharks’ll bite, but they’ll only find rock!

 

 

I found myself singing as I stumbled over the lake. The drinking song came awkwardly to me, with its bouncy tune and aggressive rhymes. It was a favorite in Astalon—Caereith’s neighboring realm. It was a classic in human lands; a human bard performing back at home once told me this, despite the fact that the song featured a troll.

It was a pretty great song if you ignored the sleazy lines after the chorus.

Unfortunately, I was a terrible singer. I sang and rhymed, desperately trying to channel Galesong into my voice. But it was weak. My voice wasn’t trained, and I couldn’t put all the nuances Galesong required into my sound. Instead of a smooth, silky tone, my voice leaked out like water from a creaky tap, squeaking and straining. Failing to channel the song of the wind.

So, I staggered around like an idiot, walking atop the water, flickering between weightlessness and being the seven-foot-tall lump of muscle that I was.

 

A Troll, a Walker, a man with skin o’ stone

A brawler, a brute, a bottle-caged storm

With arms like mountains an’ metal fer bone

Sir Randy the Walker fears one thing alonE—

 

My voice cracked at the final note. The Galesong collapsed. Wind slammed into me from the left, throwing me into the water as my weight returned. The lake swallowed me. My face met the mud at the bottom, and I sputtered out of the shallow, cursing, coughing. Gagging.

And Venti hovered over me, chirping out her chime-like bird’s laugh.

“Go on, laugh some more, will you!”

Glaring, I wiped the mud from my face and flicked it at the bird. She dodged with ease and sang, and the wind spun in a curve, deflecting the mud straight back towards me. A wet slap struck my forehead as grainy wetness exploded over my thorned brows.

As it usually did.

Chagrined, I sighed and wiped the muck from my face.

Ahead of us, our glekk captive looked on with a look of disgust twisting the corners of its mouth. I squeezed the hem of my tattered shirt dry and glared.

“What? You plan on slinging mud at me too? Just try it.”

The glekk shook its head and turned, resuming its march back towards its home. The water was shallower, now. In some areas, patches of grass and mud poked through the surface. The glekk jerked a webbed thumb towards one such mound.

“Mud face good,” it replied. “It hide ugly.”

“It must be quite good on you, then.”

“You stupid. Dumb rock thing.”

“Your mother’s a carp.”

It snarled at me and turned away, stomping away faster than before.

I smirked. That was right. Run. No one won against me in playground insults. When it came to pettiness, I was king. Once, I’d walked into a tavern I hated just to throw a sack full of rats inside. I’d spent the whole week catching and feeding them, preparing my revenge. All because I heard the owner complaining about father.

Well, he got what was coming. That infestation lasted for years.

With my help, of course.

The memory kept my spirits up as I brought the bansuri back to my lips, playing a tune once again. It wasn’t one that I already knew, or even one that channeled the voices of the world.

There no magic in it.

It was just a pleasant sound.

I danced a little as I played—swaying to the tune. Venti perched on my shoulder and sang along with her own little chime. Improvised. Natural.

It was nice.

My fingers danced over the tone holes as we continued our march.

It had been three days since I separated from the caravan, and the journey was proceeding without interruption. We walked normally at day, we traveled with Galesong at sun’s crest. At night, we rested around a crackling fire. Of the three of us, Venti and I required no food. So, it was usually us watching the glekk sit in the water, waiting for fish to catch.

The monster in question trudged out ahead of us. It walked towards the distance, where the faint outline of trees made themselves apparent over the horizon’s line.

They were shaded with deep reds and purples. The fall colors in Caereith. Even as far out as we were, I saw a few stray leaves floating along the lake. Occasionally, we passed tall patches of grass and cattails. Redbriar. The water thinned further and my feet began squelching against mud. Stepping over the water-soaked roots, I ran my hand over a patch of shyfern.

The little, peacock-tail bushes wilted underneath my touch, folding, shying away from contact. It sank into the water with a plop and promptly disappeared.

A grin flashed onto my face, then dampened a little right after.

It was a shame, witnessing all these things without the rest of my senses in full.

The colors were dulled to me, and the air was tasteless. Fresh water was just… wet. It wasn’t cold or refreshing or pleasant. Just water. Now that my senses were effectively neutered, the world was colorless to me in many ways. Ones beyond simple sight and smell.

So, I did what I could to fill the gaps.

I played my songs. I expressed colors through sounds. I sang sensations into the air.

My bansuri allowed me to do it. It eased the sadness of a thousand, grey-shaded miles. My traveling group of three stopped at the base of a large bald cypress. It speared into the sky. Crawling roots dug into the earth, and thin branches clutched out towards the clouds. The tree’s crown of red leaves quietly shed its many children into the lake.

We stopped to rest underneath it as the sun began to set.

As usual, Venti flew off to do her duty as a sunbird. She soared into the trees, sprinkling light into the swamp. It left me and the glekk alone in the camp, waiting for night to pass. Our captive huddled up between the roots, eyeing me with its dead eyes. A campfire crackled between us.

“Home, near,” it gurgled. “Me lead. You let go, me after.”

I rolled my eyes. “Yeah, yeah,” I replied. “You’re free to go once you take me there. I have no reason to keep you prisoner after that. What, did you think I liked having you around?”

“Me not like you too.”

“And that’s why I’m going to rob your tribe.”

“Pest! You!” the glekk snarled at me, “You die when me gone. I run. Before bad thing come.”

I furrowed my brows. Bad thing this, shadow thing that. What the rot was this fish on about? I sat down on a patch of mud in front of it, resting an elbow against the roots. I motioned towards it.

“You really aren’t shutting up about this shadow thing. What even is it?”

The monster grinned at that, and I was once again reminded of why glekk were killed on sight. There was pure hatred in its eyes. A complete, and utter wish for me to suffer and die. It even looked amused at the concept of it, as if a gruesome death were a roadside attraction for it to enjoy.

“You, die. Soon,” it said. “You call shadow thing. With stick. You song to tall plant mans. Fleshy mans. Save them with stick-sound. It hear you. It listen. Shadow thing come.”

I frowned. The sun was halfway down the horizon, now. And darkness fell. The gloom stretched, and the silhouettes of the branches around us suddenly looked like a thousand shadowy arms, all reaching towards me.

I shook my head, dispelling the thought. This thing was just trying to scare me. What was I, six? Such a stupid tactic wasn’t going to work. I scoffed at it and looked away.

“Ridiculous. You think something’s going to come kill me just because I played my…”

I stopped. Words from months ago—words that I’d forgotten—slipped into my mind. I remembered what the Fae said. ‘The songs you play, child,’ he’d told me. ‘They are dangerous and powerful, and the man who sings them calls to things darker than even the Winter Court.’

As if reading into my silence, the glekk grinned wider.

“Shadow come soon,” the glekk said, “But you die. Sooner. Before shadow come. You die now. And bird die first.”

Something about its words sent a chill up my spine.

“Venti? Don’t be ridiculous. Nature spirits can’t be killed.” I stood up, looking into the forest. It was dark, now. And my grey-tinted night vision failed to penetrate deep into the thick shadows of the wood. I played a sharp note on my bansuri—a birdcall. I called out into the swamp. “Venti! Are you there?”

No answer.

The sun was already gone. And alone, in the dark, I was made all too aware of how exposed I was. Every creak in the woods made me turn. Every subtle shift in the shadows made my skin crawl. I felt eyes on me. Everywhere. Watching.

This wasn’t normal.

Gritting my teeth, I walked up to the glekk and grabbed it by the neck. I slammed it into the tree. “This isn’t your home, is it?” I asked, and it cackled its gurgling laugh. I slammed it into the bark again. Cut the laugh short. I loomed over it, growling, “Where did you lead us? Where’s Venti?”

The glekk grinned with teeth. Its eyes moved to something above us.

A shadow loomed over me.

My instincts screamed at me, then. But my body was frozen. It was like I was encased in molasses, the fear turning into a thick, viscous liquid that saturated the air. It slowed me. Made me weak. I heard the faint sound of whispers in the air. The voice of the world, sinister, woven into a cursed that sapped at my strength.

The sixth sense I cultivated began to disappear as the rest of my senses returned. I smelled the air. Felt the cold. The darkness turned black, and the colors returned. Orange firelight shaded the trees.

I felt my mortality call to me.

Something was casting magic. The evil kind.

Shivers rolled down my spine. I felt myself look up. Slowly. To where the glekk was staring.

I didn’t know what I was seeing, at first, stretched as it was. And then I saw the wrinkles. The black, rotted gums. And then the sharp teeth, scything into a massive, massive smile. I saw the thing’s hooked nose and dark eyes, and its pinprick irises staring down at me.

A face cast in shadow looked down at from above, leaning over the side of the cypress. Long, thin fingers curled over the edge of the tree. Each as long as my arm.

The thing behind the tree slowly stepped out of the shadows.

It was spider-like. Gangly and thin. All wrinkled flesh, hanging limbs, and impossible proportions.

The face of a pale, old woman grinned down at me. Black hair flowed like tar. Her neck strung itself into a skeletal torso with shriveled, sagging breasts. Then a bloated belly coated in filth. Its limbs snaked down, long and thin arms and fingers, long enough to reach the water below. It walked with a hunch and a camel’s humped back, each footstep leaving massive prints in the mud. Mushrooms grew from its skin. Insects crawled in her flesh.

I stood, frozen. Watching every terrible inch of it emerge from the shadows.

The giant Hag leaned forward, its face as wide as I was tall. It drew close to me, and I smelled the rot in her breath. The cavities in her teeth. I felt myself breathing quickly, the fear threatening to burst out of my chest as recognition dawned.

Hags were stories. They were myths. They weren’t supposed to exist.

And yet here she was.

Long, thin fingers touched my cheek. They moved to my chin and raised my head. Forced me to meet her eyes. Sharp fingernails tapped against my throat.

“My, my,” she whispered, her voice a chorus of wet growls. As if phlegm was clogging her throat. “Guests are rare in this side of the wood. And especially ones so... young. Did you bring this gift, lake child?”

Her eye snapped to the glekk. Fast. It nodded, shaking. My trembling hands slowly let go of its throat, and the glekk splashed down into the water, covering its mouth. Suppressing its coughs. The Hag’s long, blistered tongue emerged from her mouth in deliberation. She ran it over her cracked lips, and observing me, her grin turned into a frown. Dark and serious.

The Hag peered into me and she looked unhappy with what she saw.

She waved the glekk away.

“Leave.”

The glekk scampered away without another word. It ran, disappearing into the trees within moments. And it left me, alone in the dark, in front of the thing all parents taught their children to fear.

The Hag.

I felt my legs wobble beneath me. I fell with a splat on the mud, and suddenly I was a child again. I was a scared kid, staring into the shadows and expecting the darkness to stare back. And now that it actually met my gaze, I didn’t know what to do. I could do nothing but sit. Wait for death to come. The Hag opened its mouth. Spoke. Its words didn’t register in my head.

Instead, I focused on her sharp, sharp teeth. Needlepoint and endless, running down in rows that spiraled down her throat. I looked up at it.

At her.

She was so very tall.

I watched the Hag reach into a rotted leather satchel in her waist. She brought out a globe of black glass, transparent despite reflecting no light. Inside, a listless bird of blue light was slumped against the container.

Venti.

The ringing in my ears cleared. My senses focused. I heard the Hag speak.

“Is this tasty little morsel yours, child? A pet?”

“…My partner,” I said, but my voice came out a whisper. Quiet and afraid. The Hag sneered in distaste, throwing the orb of black glass back into her satchel.

“Partners. With a nature spirit, he says. Hah!” she cackled ruefully, turning. The sound of a laugh stunned me out of my fear, and the Hag tapped the satchel with a bony finger as she faced the darkness. “The bird’s scent reeks of Fae. It stinks up my swamp. She stays in the bottle until I decide whether to kill you for dragging your problems onto my doorstep.”

“Problems?” I asked, trying to rise. “W-What?”

“Your pursuers, child. They swarm to us like worms to a corpse.”

She brought out a massive raven’s feather from her bag. The Hag glanced at me, then scoffed.

“Run, Nameless boy. We either flee now, or die to the shadows that chase you.”

I watched the Hag clench her fist around the feather, and her flesh twisted. Flesh tore and bones cracked and popped and shifted inside of her. Within seconds, I watched in horror as her flesh deformed. As feathers sprouted from her skin. I watched her hooked nose harden into a black beak, and her arms turn into black-feather limbs.

The giant raven-Hag spread her wings.

“Flee to the corpse of the Ancestor Tree north. The hungry things draw near, and I will not wait for them to find me. The sky vanishes now—there is nothing I can do to save you.”

She looked up at the sky, and I followed her gaze. My back chilled.

There were no stars. Only darkness.

Something fell from the heavens. It splashed into my cheek. I touched it, and my fingers came away black as if smeared with ink. It felt cold to the touch. Colder than ice; a sensation as sharp as steel’s edge.

I stared at it in confusion.

“What is—"

“Pity, for a new immortal to die so young,” she said, cutting me off. “I prepare tea at home for guests to drink. Do drop by. If you survive, that is.”

She cackled out another laugh. But it was gruesome. Perhaps even a little afraid. I turned to her, my brain failing to even process what was happening, and I opened my mouth to ask—to demand answers. But the Hag wasn’t in the mood to explain. She flapped her wings once and the wind surged. Water splashed. Her raven form shot into the sky and blurred north without another word.

A living nightmare of Caereith, feared by all. And she just… fled.

Because a bigger nightmare than her was coming.

And it was coming for me.