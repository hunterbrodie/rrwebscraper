use select::document::Document;
use select::predicate::{Class, Name, Predicate};
use std::fs::File;
use std::fs;
use std::io::prelude::*;
use std::io::BufReader;
use std::env;
use std::path::PathBuf;

use serde_json::json;
use serde::{Deserialize, Serialize};

use std::thread;

#[derive(Serialize, Deserialize)]
struct MetaData
{
	url: String,
	chap_num: usize,
	last_read: i32,
}

fn main()
{
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 || args.len() != 2
    {
        update("downloads".to_owned());
    }
    else
    {
        let results = search(&args[1]).unwrap();

        if results.len() == 0
        {
            println!("no results");
        }
        else
        {
            for x in 0..results.len()
            {
                println!("[{}] {}", x, results[x].0);
            }
            
            let mut input_text = String::new();
            std::io::stdin()
                .read_line(&mut input_text)
                .expect("failed to read from stdin");
        
            let trimmed = input_text.trim();
            match trimmed.parse::<usize>() {
                Ok(i) => 
                {
                    let url = format!("{}", results[i].1);
                    let dir = format!("downloads/{}", &results[i].0);
                    init_scrape(url, dir);
                },
                Err(_) => println!("failed to read input"),
            };
        }
    }
}

fn search(title: &str) -> Result<Vec<(String, String)>, Box<dyn std::error::Error>>
{
    let url = format!("https://www.royalroad.com/fictions/search?title={}", title);

    let resp = reqwest::blocking::get(&url)?
        .text()?;

    let document = Document::from(resp.as_str());

    let mut results: Vec<(String, String)> = Vec::new();

    for node in document.find(Class("fiction-title").descendant(Name("a")))
    {
        let tuple = (node.text().clone(), String::from(node.attr("href").unwrap()));
        results.push(tuple);
    }

    Ok(results)
}

fn init_scrape(url: String, dir_str: String)
{
    let mut url: String = format!("https://www.royalroad.com{}", url);

    let client = reqwest::blocking::Client::new();
    let resp = client.get(&url)
        .send()
        .unwrap()
        .text()
        .unwrap();
    
    let document = Document::from(resp.as_str());

    for node in document.find(Class("fic-buttons").descendant(Name("a"))).take(1)
    {
        url = format!("https://www.royalroad.com{}", node.attr("href").unwrap());
    }

    let tuple = scrape_internal(url);
    let chapters = tuple.0;
    let link = tuple.1;

    let mut path = PathBuf::from(dir_str);
    let mut i = 1;
    let filename = path.file_name().unwrap().to_str().unwrap().to_owned();

    loop
    {
        if path.exists()
        {
            path.pop();
            path.push(format!("{}({})", &filename, i));
            i += 1;
        }
        else
        {
            fs::create_dir_all(&path).unwrap();
            break;
        }
    }

    
    for i in 0..chapters.len()
    {
        path.push(format!("{}.chapter", i + 1));
        let mut file = File::create(&path).unwrap();
        file.write_all(chapters[i].as_bytes()).unwrap();
        path.pop();
    }

    path.push(".meta");
	let mut file = File::create(&path).unwrap();

	file.write_all(json!(MetaData
	{
		url: link,
		chap_num: chapters.len(),
		last_read: 1,
	}).to_string().as_bytes()).unwrap();
}


fn update(dir: String)
{
	let stories = fs::read_dir(&dir).unwrap();
	let mut handles: Vec<std::thread::JoinHandle<()>> = Vec::new();

	for p in stories
	{
		let p = p.unwrap();
		handles.push(thread::spawn(move ||
		{
			update_story(&mut p.path());
		}));
	}

	for handle in handles
	{
		handle.join().unwrap();
	}
}

fn update_story(path: &mut PathBuf)
{
	path.push(".meta");
	let mut input = File::open(&path).unwrap();
	let mut data_str = String::new();
	input.read_to_string(&mut data_str).unwrap();
	
	let mut data: MetaData = serde_json::from_str(&data_str).unwrap();

	path.pop();

	let client = reqwest::blocking::Client::new();
	let resp = client.get(&data.url)
		.send()
		.unwrap()
		.text()
		.unwrap();
		
	let document = Document::from(resp.as_str());
		
	for node in document.find(Class("chapter-page").descendant(Class("nav-buttons")).descendant(Class("col-lg-offset-6")).descendant(Class("col-xs-12"))).take(1)
	{
		match node.attr("disabled")
		{
			None => data.url = format!("https://www.royalroad.com{}", node.attr("href").unwrap()),
			Some(_) => data.url = String::new(),
		};
	}

	if !data.url.is_empty()
	{
		let tuple = scrape_internal(data.url);
		let chapters = tuple.0;
		let link = tuple.1;

		for i in 0..chapters.len()
		{
			path.push(format!("{}.chapter", i + data.chap_num + 1));
			let mut file = File::create(&path).unwrap();
			file.write_all(chapters[i].as_bytes()).unwrap();
			path.pop();
		}

		data.url = link;
		data.chap_num += chapters.len() - 1;
	
		path.push(".meta");
		let mut file = File::create(&path).unwrap();
		file.write_all(json!(data).to_string().as_bytes()).unwrap();
	}
}


fn scrape_internal(url: String) -> (Vec<String>, String)
{
    let mut url = url;
    let mut links = true;
    let mut chapters: Vec<String> = Vec::new();
    let client = reqwest::blocking::Client::new();

    while links
    {
        let resp = client.get(&url)
            .send()
            .unwrap()
            .text()
            .unwrap();
    
        let document = Document::from(resp.as_str());
    
        for node in document.find(Class("chapter-content")).take(1)
        {
            chapters.push(str::replace(&node.text().trim(), "\n", "\n\n"));
        }
    
        for node in document.find(Class("chapter-page").descendant(Class("nav-buttons")).descendant(Class("col-lg-offset-6")).descendant(Class("col-xs-12"))).take(1)
        {
            match node.attr("disabled")
            {
                None => url = format!("https://www.royalroad.com{}", node.attr("href").unwrap()),
                Some(_) => links = false,
            };
        }
    }

    (chapters, url)
}